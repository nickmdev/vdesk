export class Product {
  public id: number;
  public name: string;
  public sizeOptions: any[];
  public colorOptions: any[];
}
