# VDESK App #
Sample app showcasing different Angular features.

Live link: http://ng-vdesk-deployment.s3-website.us-east-2.amazonaws.com/

### Order Module ###
You should separate application features into their own modules for optimization. This helps improve maintainability and allows for lazy-loading implementation.

### Fake Backend Service ###
Implements the Angular HttpInterceptor class to allow the application to run without a backend. By extending the class, you can create custom interceptors to modify http requests before they get sent to the server. I use this to intercept requests based on the URL to provide a fake response for getting product data by id. This is great for testing as the controller and the service remained unaware of the interceptor.
https://angular.io/guide/http

### Reactive Forms ###
The PaymentComponent uses Reactive forms, which is an Angular technique for creating forms in a reactive style.
https://angular.io/guide/reactive-forms

### Angular Flex-Layout ###
I used the FlexLayoutModule to create a responsive site. The module provides HTML UI layout for Angular applications; using Flexbox and a Responsive API.
https://github.com/angular/flex-layout

### Route Animations ###
Angular 4.2 introduced access to the router-oulet/ActivatedRoute allowing for easier route animation setup. I utilized this to perform animations on route changes.

### Limitations ###
This was built pretty quickly so I only tested in Chrome. It definitely will not run in IE as I didn't include the necessary polyfills. The Review page wasn't completed and the Confirmation page was pretty phoned in... lots of inline styling.
